// setting up some variables

// Requiring the dependencies
var sfdc =  require('sfdc-node') 
var express = require('express');
var S = require('string');
var jpath = require('JSONPath').eval;
var log4js = require('log4js');

// configure our logger, leave the logfiles out for the moment
log4js.configure({
        appenders: [
                { type: 'console' },
//                { type: 'file', filename: 'cheese.log', category: 'cheese' }
        ]
});


var log = log4js.getLogger('cheese');
var logLevel = process.env.LOGLEVEL || 'DEBUG';
log.setLevel(logLevel);

//OAuth Configuration
var NSclientId = process.env.CLIENTID || '3MVG9QDx8IX8nP5QCEQeRdOeorP6KSnQaPGgm87BnTEOG8KwxsbtlEkj843qf1hvkpekxSRvOZthXIRyjcU2y'; 
var NSclientSecret = process.env.CLIENTSECRET || '1490437775653757861'; 
var NSRedirectUrl = process.env.REDIRECTURL || 'http://intense-brook-6811.herokuapp.com/oauth2/callback'; 

// making sure the correct OAUTH parameters are used. 
// vars shoudld be filled with heroku env variables
log.debug("NSclientId :" + NSclientId);
log.debug("NSclientSecret :" + NSclientSecret);
log.debug("NSRedirectUrl :" + NSRedirectUrl);

//These are set after authorize is called
var token;
var instance;

// setting up the web server
var app = express.createServer();
var port = process.env.PORT || 3000;

// configure the app server
app.configure(function(){ 
  log.info("starting configuration load");
  app.use(express.logger('dev'));

  //Default Express based application configuration
  app.set('views', __dirname + '/views');
  
// we will be using the jade engine
// with activated layouts. If you want to change the look and feel, checkout views/layout.jade
  app.set('view engine', 'jade');
  app.set('view options', {
    layout: true
    });
  app.use(express.bodyParser());
  app.use(express.methodOverride());
  app.use(function(req, res, next) {
    res.setHeader('X-Frame-Options', 'ALLOW-FROM https://na12.salesforce.com');
    next();
  });
  app.use(app.router);
  app.use(express.cookieParser());
  app.use(express.session({'secret' : NSclientSecret}));


  // configure the public directories that serve images and stylesheets
  app.use(express.static(__dirname + '/public'));
  app.set('styles', __dirname + '/public/stylesheets')
  app.use(express.favicon('/public/favicon.ico'));
  
// we'll be using router
  app.use(app.router);
  log.info('Listening on port: ' +port);
  log.info('Config loaded');

  //pass the app and callback path to configureCallback to set up the route for the callback for the 
  //sfdc remote access authentication as well as a method to call when the call back happens

  // important part is MSCallback - that's the function that will be called when we're authenticated
  // We'll get all sorts of variables with that call, e.g. id of user, oauth token etc.
  sfdc.configureCallback(app, '/oauth2/callback',NSRedirectUrl, function(access_token, refresh_token, results, res){

// YAY we made it, if this function gets called, we're authenticated and know wo did it

  // if clause to avoid exceptions & setting the important variables
  if(results){
    token = access_token;
    instance = results.instance_url;
    code = results.code;
  
  // logging it for the console
    log.info("app.js: token :"+ token);       
    log.info("app.js: instance :"+ instance);   
    log.debug("app.js: code :"+ code);   
    log.debug("app.js: results from sfdc.configureCallback :"+ JSON.stringify(results));
    }
  else{
    // seems like there was an error in rest.js, since results was empty
    log.error("results was undefined");
  }
  
  // Now this is where we do the cool stuff such as retrieving user info and calling the rest api

  // We're calling the api to find our who the user is
  var userId = S(results['id']).right(18).s;
  log.info('app.js 92: userId :' +userId);
  var query = 'SELECT Id, FirstName, LastName, Email FROM User WHERE Id = ' + "'" + userId + "'";
  log.debug('app.js 94: query :' +query);
   
  sfdc.query(query, token, instance, function(results){
  log.debug('app.js 97: stringified results :' + JSON.stringify(results));

  // we'll redirect to the page where the action happens and handover whatever we got back from the API  
  // Add info that we haven't retrieved yet and restructure results for jade
  // composing a string with the required user information
  var myString = '{"FirstName": "'+ jpath(results, '$..FirstName') + '","LastName": "'+ jpath(results, '$..LastName') +'","Email": "' + jpath(results, '$..Email') +'","AssignedGoals": ' + 2 +',"CompletedGoals":'+ 2 +'}';
  log.debug('composed String before objectifying: ' +myString);
  var object = JSON.parse(myString);

  res.locals.results = object;
  log.error('stringified res.locals.results: ' + JSON.stringify(res.locals.results));
  });

  // getting all assigned goals for that user
  query = 'SELECT Id, Name, pocManaged__Status__c, Badge_for_Completion__c, Name_of_the_Resource_File__c FROM pocManaged__Goal__c WHERE OwnerId = ' + "'" + userId + "'";
  sfdc.query(query, token, instance, function(results){

    // let's see what we got back
    log.debug('app.js 126: stringified goals :' + JSON.stringify(results));
    
    // splitting the goals in two different arrays to see how many have been assigned vs. completed
    var assignedGoals, completedGoals = new Array();
    assignedGoals = jpath(results, '$..records[?(@.pocManaged__Status__c)]');
    for (var i = 0; i < assignedGoals.length; i++) {
      var goal = assignedGoals[i];
      if (goal.pocManaged__Status__c == "Signed Off") {
        completedGoals.push(goal);
      };
    };

    log.debug("app.js 131: assignedGoals: " +JSON.stringify(assignedGoals));
    log.debug("app.js 132: completedGoals: " +JSON.stringify(completedGoals));
    
    // we're setting the numbers of the goals to display on the callback page
    if (res.locals.results) {

      var stringGoals = '[{"attributes":{"type":"pocManaged__Goal__c","url":"/services/data/v22.0/sobjects/pocManaged__Goal__c/a00U0000004o200IAA"},"Id":"a00U0000004o200IAA","Name":"Create a Chatter Group","pocManaged__Status__c":"Signed Off","Badge_for_Completion__c":"Send a private message to another user","Name_of_the_Resource_File__c":null},{"attributes":{"type":"pocManaged__Goal__c","url":"/services/data/v22.0/sobjects/pocManaged__Goal__c/a00U0000004njoTIAQ"},"Id":"a00U0000004njoTIAQ","Name":"Another: Write a chatter post","pocManaged__Status__c":"Signed Off","Badge_for_Completion__c":"Send a private message to another user","Name_of_the_Resource_File__c":null}]'
      var objectifiedGoals = JSON.parse(stringGoals);
      res.locals.results["Goals"] = stringGoals;
      res.locals.results["AssignedGoals"] = assignedGoals.length;
      res.locals.results["CompletedGoals"] = completedGoals.length;
    };
    
    // just checking whats handed over
    log.error('app.js 136: res.locals.results: ' +JSON.stringify(res.locals.results));
  });

  // calling callback after we have retrieved the API information
  res.redirect('/callback/'+userId);
  });
});

// defining the port for the webserver
// webserver fully set up
app.listen(port);
log.info('Express server started on port %s', app.address().port);




// Call the authorize method to process the inital callback with the code.  
// The OAuth library does not process 302s.  
// Salesforce returns a 302 to push the user to login page
app.get('/login', function(req, res){
    // experimental to see if I can query the login url from the req, seems like its needed for canvas
    var loginUrl;
    if (req.query["loginUrl"]) {
      return res.render('login.jade', { loginUrl: req.query["loginUrl"] });
    } else{ 
      loginUrl = 'https://login.salesforce.com';
    }

    sfdc.authorize(NSclientId, NSclientSecret, loginUrl, NSRedirectUrl, 'popup', function(result, statusCode, location){
      
      // HACK: Need to call back to authorize to process the 302.  
      // OAuth library does not do this automatically
        if(statusCode == 302){

          // salesforce wants to authenticate, therefore redirect to 302
            res.redirect(location, 302);
        } else {

          // seems like something went wrong - easy way out to /error
          log.debug('result :' +result);
          res.redirect('/error');
        }  
    });
});


// configure the routings inside the app
// The homepage, people will see this when unauthenticated
app.get('/', function(req, res){
  log.debug('log parameters on index :' +req.query);
	log.info('render index');
    res.render("index.jade");
});

// the error page currently 404
app.get('/error', function(req, res){
  log.info('render error');
    res.render("error.jade");
});


// Users only get here when they're authenticated
// displays goals as well as 
// display the users information

app.get('/callback/:id', function(req, res) {
    

// turning JSON into strings and handing it to the jade template
var userInfo = res.locals.results;

log.error('app.js 224: userInfo: ' + JSON.stringify(userInfo));
res.render('callback.jade', {userInfo: userInfo});
});


app.get('/test', function(req, res){
  var userInfo = res.locals.results;
  log.debug(userInfo);
  res.render('callback.jade', {'userInfo': userInfo});
})


app.get('/oauth2/callback', function(req, res){
  log.info('here to callback and window should close');
  res.render('popupclose.jade');
})

/// app.post to check out Signed Request
var oauth; 

app.post('/', function(request, response){

    var reqBody = request.body.signed_request;   
    var requestSegments = reqBody.split('.');    
    var requestContext = JSON.parse(new Buffer(requestSegments[1], 'base64').toString('ascii'));
    var userInfo;

    oauth = new Object();
    oauth.access_token = requestContext.oauthToken;
    oauth.instance_url = requestContext.instanceUrl;

    if (requestContext) {
      //console.log("context.userId :" +requestContext.userId);
      //console.log("context :" + JSON.stringify(requestContext));
      console.log("userFullName :" +jpath(requestContext, '$.context.user.fullName'));
      console.log("userEmail :" +jpath(requestContext, '$.context.user.email'));
    };   

    console.log("oauth.instance_url :"+oauth.instance_url);
    console.log("oauth.access_token :" +oauth.access_token);

    var myString = '{"FirstName": "'+ jpath(requestContext, '$.context.user.firstName') + '","LastName": "'+ jpath(requestContext, '$.context.user.lastName') +'","Email": "' + jpath(requestContext, '$.context.user.email') +'","AssignedGoals": ' + 2 +',"CompletedGoals":'+ 2 +'}';
    log.info('composed String before objectifying: ' +myString);
    userInfo = JSON.parse(myString);


    response.render('callback.jade', {'userInfo': userInfo});
});

var nforce = require('nforce');
io = require('socket.io').listen(app);
io.configure(function () { 
  log.debug('sockets.io configure');
  io.set("transports", ["xhr-polling"]); 
  io.set("polling duration", 10); 
});



  //when a new socket.io connection gets established
  io.sockets.on('connection', function (socket) {
    log.debug('subscribing to topic');
      
    try
    {
      console.log('connect to topic');
      log.debug('sent the oauth token: '+ JSON.stringify(oauth));
      //create connection to the GoalsForUser push topic.
      var str = org.stream('GoalsForUser', oauth);
    
      //on connection, log it.
      str.on('connect', function(){
        console.log('connected to pushtopic');
      });
    
      str.on('error', function(error) {
         socket.emit(error);
      });
    
      //as soon as our query has new data, emit it to any connected client using socket.emit.
      str.on('data', function(data) {
        console.log('received new data');
         socket.emit('news', data);
      });
    }
    catch(ex)
    {
      log.debug('caught exception');
        console.log(ex);
    }
    
  });


